import { Employee_data, TimeSheetData } from "../Utils/mongo_db.js";
import { jwtgenerator } from "../Utils/jwtgenerator.js";

/**
 * generate password function generates random password
 */

function generatePassword() {
  var length = 8,
    charset =
      "abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+0123456789!@#$%^&*()_+",
    password = "";
  for (var i = 0, n = charset.length; i < length; i++) {
    password += charset.charAt(Math.floor(Math.random() * n));
  }
  return password;
}

/**
 * post employee details post the employee details into the database
 * and validates the body params before pushing into the database
 * @param {*} request is the the parameters passed from the database
 * @param {*} response is the response based on the data sent from the user
 */
const createemployee_handler = (request, response) => {
  const login_data = request.body;

  const name = request.body.name;
  const employeeId = request.body.employeeId;
  const phonenumber = request.body.phonenumber;
  const role = request.body.role;
  const email = request.body.email;
  const password = generatePassword();
  const secret = request.body.secret;
  //validating the password using _validate_name()

  const post_data = {
    name: name,
    employeeId: employeeId,
    phonenumber: phonenumber,
    email: email,
    role: role,
    password: password,
    secret: secret,
  };

  const posts_data = new Employee_data(post_data);
  posts_data.save().then(() => response.send(JSON.stringify({
    status_code:200,
    status_message:"DATA added"
  })));
};

/**
 * login handler function validates the login data given by the client
 * and if the user gives the proper parameters then it pushes the data into the database
 * @param {*} request is the parameters given by the user
 * @param {*} response is the response based on the data sent by the user
 */

const login_user_handler = (request, response) => {
  const login_data = request.body;
  if (Object.keys(login_data).length !== 3) {
    response.send({
      status_code: 406,
      status_message: "All input fields are Required",
    });
  }
  let body_keys_list = [];
  for (let item in login_data) {
    body_keys_list.push(item);
  }
  //delaring a list of keys to validate the keys
  let key_list = ["employeeId", "password", "role"];

  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    }
    //if any key is missing it throws some error msg
    else {
      let msg = key + " key is Missing!";
      response.send({ status_code: 406, status_message: msg });
    }
  }
  Employee_data.find({ employeeId: login_data.employeeId }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      if (data.length !== 0) {
        for (let item of data) {
          if (item.password === login_data.password) {
            login_data.status_code = 200;
            const jwt_token = jwtgenerator(login_data.employeeId);
            login_data.jwt_token = jwt_token;
            response.send(JSON.stringify(login_data));
            break;
          } else {
            const obj = {
              status_code: 401,
              status_message: "invalid password",
            };
            response.send(JSON.stringify(obj));
            break;
          }
        }
      } else {
        const obj = {
          status_code: 404,
          status_message: "username not found",
        };
        response.send(JSON.stringify({
          status_code:200,
          status_message:"DATA added"
        }));
      }
    }
  });
};

const post_timeSheet_handler = (request, response) => {
  var request_params = request.body;
  const TimesheetDetails = new TimeSheetData({
    employeeId: request_params.employeeId,
    Hours: request_params.Hours,
    Description: request_params.Description,
    Date: request_params.Date,
  });
  TimesheetDetails.save().then(() => console.log("data saved"));
};

//exporting the functions
export { createemployee_handler, login_user_handler, post_timeSheet_handler };
