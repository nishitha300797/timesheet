import { Employee_data, TimeSheetData } from "../Utils/mongo_db.js";

/**
 * get employee details fetch all the employee details
 * @param {*} request is the request by the user
 * @param {*} response is the response with respect to the user
 */
const get_employee_details = (request, response) => {
  Employee_data.find({})
    .then((data) => {
      response.send(data);
    })
    .catch((err) => {
      console.log(err);
    });
};

const get_employee_details_by_id = (request, response) => {
  let id = request.params.id;
  Employee_data.find({ employeeId: id })
    .then((data) => {
      response.send(data);
    })
    .catch((err) => {
      console.log(err);
    });
};
/**
 * fetch timesheet details with respect to id
 * @param {*} request is the request by the user
 * @param {*} response is the response with respect to the user
 */
const get_time_sheet_handler_by_Id = (request, response) => {
  let id = request.params.id;
  TimeSheetData.find({ employeeId: id })
    .then((data) => {
      response.send(data);
    })
    .catch((err) => {
      console.log(err);
    });
};

export {
  get_employee_details,
  get_time_sheet_handler_by_Id,
  get_employee_details_by_id,
};
