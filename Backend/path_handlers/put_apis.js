import { Employee_data } from "../Utils/mongo_db.js";

const forgot_user_handler = (request, response) => {
  const forgot_data = request.body;
  if (Object.keys(forgot_data).length !== 3) {
    response.send({
      status_code: 406,
      status_message: "All input fields are Required",
    });
  }
  let body_keys_list = [];
  for (let item in forgot_data) {
    body_keys_list.push(item);
  }
  //delaring a list of keys to validate the keys
  let key_list = ["employeeId", "newpassword", "secret"];

  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    }
    //if any key is missing it throws some error msg
    else {
      let msg = key + " key is Missing!";
      response.send({ status_code: 406, status_message: msg });
    }
  }

  const newpassword = request.body.newpassword;

  const employeeId = request.body.employeeId;
  const secret = request.body.secret;

  const x = async () => {
    try {
      const result = await Employee_data.updateOne(
        ({ employeeId: `${employeeId}` }, { secret: `${secret}` }),
        {
          $set: {
            password: `${newpassword}`,
          },
        }
      );
      console.log(result);
      response.send(JSON.stringify("password updated successfully"));
    } catch (err) {
      console.log(err);
    }
  };
  Employee_data.find({ employeeId: `${employeeId}` }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      if (data.length !== 0) {
        x(employeeId);
      }
    }
  });
};

export { forgot_user_handler };
