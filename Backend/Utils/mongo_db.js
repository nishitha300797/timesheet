// importing mangoose from mangoose
import mongoose from "mongoose";

// importing schema from schema.js
import { Employee_schema, TimeSheetSchema } from "./schema.js";

//creating the model for employee data
const Employee_data = mongoose.model("Employeedata", Employee_schema);

//creating the model for time sheet data
const TimeSheetData = mongoose.model("TimeSheetData", TimeSheetSchema);

//exporting the models
export { Employee_data, TimeSheetData };
