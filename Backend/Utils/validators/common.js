/**
 * validates the string parameter if it is in valid format
 * @param {1} email which has to be validated
 * @returns an array of strings of error message
 */
function validate_email(email) {
  let returnArray = [];

  if (!email) {
    // check if parameter is sent
    returnArray.push("Email has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof email != "string") {
    // check if param is string
    returnArray.push("Email parameter sent is not a string " + email);
    return returnArray;
  }
  if (
    !email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    // check if param is valid format
    returnArray.push("Email is not in the right format " + email);
    return returnArray;
  }

  return returnArray;
}

/**
 * To validate the name given by the user for registering
 * @param {1} name to be validated
 * @returns an array of strings of error messages
 */
function validate_name(name) {
  let returnArray = [];

  if (name === undefined) {
    // check if parameter is sent
    returnArray.push("name has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof name != "string") {
    // check if param is string
    returnArray.push("name parameter sent is not a string " + name);
    return returnArray;
  }
  if (!name.match(/^([a-zA-Z]){4,15}$/)) {
    // check if param is valid format
    returnArray.push("name is not in the right format " + name);
    return returnArray;
  }

  console.log(name + " is a valid name");
  return returnArray;
}

/**
 * validates the phone_number which contains the 10 digits in the number or not
 * @param {1} number which has to be validated
 * @returns 200 status_code if all conditions are satisfied,
 *          otherwise returns status_code & status_message for which the condition fails.
 */

function validate_phone_number(phonenumber) {
  let returnArray = [];

  if (phonenumber === undefined) {
    // check if parameter is sent
    returnArray.push("Phone has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof phonenumber != "string") {
    // check if param is string
    returnArray.push(
      "phonenumber parameter sent is not a string " + phonenumber
    );
    return returnArray;
  }
  if (!phonenumber.match(/^[0-9]{10}$/)) {
                                                    // check if param is valid format
    returnArray.push("Phone number is not in the right format " + phonenumber);
    return returnArray;
  }

  return returnArray;
}

/**
 * Accepts the list of keys of params recieved and also list of expected params.
 * Reports if any discrepancy.
 * @param {} req Request object of express contains body which contains all parameters
 * @param {} expected_keys_list Array of expected param keys
 * @returns a list of mismatches
 */
function validate_key_lists(request, expected_keys_list) {
  let returnArray = [];

  let body_keys_list = [];
  for (let item in request.body) {
    // fetching all keys from request body
    body_keys_list.push(item);
  }
  for (let key of expected_keys_list) {
    // checking each list against another
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      let msg = key + " is Missing!";
      returnArray.push(msg);
    }
  }
  return returnArray; // possibly empty list of mismatching params
}

/**
 * validates the role which allows only ["hr", "employee", "admin"]
 * @param {} role which has to be validated
 * @returns 200 status_code if all conditions are satisfied,
 *          otherwise returns status_code & status_message for which the condition fails.
 */

function validate_role(role) {
  let returnArray = [];
  let allowed_roles_array = ["hr", "employee", "admin"];

  if (role === undefined) {
    // check if parameter is sent
    returnArray.push("role has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof role != "string") {
    // check if param is string
    returnArray.push("role parameter sent is not a string " + role);
    return returnArray;
  }
  if (!allowed_roles_array.includes(role)) {
    // check if role is a valid one
    returnArray.push("role is not the right string " + role);
    return returnArray;
  }
  return returnArray;
}

/**
 * To validate the employee id given by the user for registering
 * @param {} employeeId to be validated
 * @returns an array of strings of error messages
 */
function validate_employeeId(employeeId) {
  let returnArray = [];

  if (!employeeId) {
    // check if parameter is sent
    returnArray.push("Employee Id has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof employeeId != "string") {
    // check if param is string
    returnArray.push(
      "Employee Id parameter sent is not a string " + employeeId
    );
    return returnArray;
  }
  if (!employeeId.match(/^[0-9]{5}$/)) {
    // check if param is valid format
    returnArray.push(
      "Employee Id is not in the right format it includes" + employeeId
    );
    return returnArray;
  }

  return returnArray;
}

/**
 * To validate the secret given by the user for registering
 * @param {1} secret to be validated
 * @returns an array of strings of error messages
 */
function validate_secret(secret) {
  let returnArray = [];

  if (secret === undefined) {
    // check if parameter is sent
    returnArray.push("secret has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof secret != "string") {
    // check if param is string
    returnArray.push("secret parameter sent is not a string " + secret);
    return returnArray;
  }
  if (!secret.match(/^[a-zA-Z ]+$/)) {
    // check if param is valid format
    returnArray.push("secret should only contain alphabets" + secret);
    return returnArray;
  }
  return returnArray;
}

function validate_password(password) {
  let returnArray = [];

  if (password === undefined) {
    // check if parameter is sent
    returnArray.push("pasword has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof password != "string") {
    // check if param is string
    returnArray.push("password parameter sent is not a string " + password);
    return returnArray;
  }
  if (!password.match(/^[a-zA-Z0-9!@#$%^&*()_+]{6,16}$/)) {
    // check if param is valid format
    returnArray.push("password is not in the right format " + password);
    return returnArray;
  }
  return returnArray;
}

export {
  validate_email,
  validate_name,
  validate_phone_number,
  validate_key_lists,
  validate_employeeId,
  validate_role,
  validate_secret,
  validate_password,
};
