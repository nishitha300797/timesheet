import { validate_email, validate_key_lists, validate_name, validate_phone_number } from "./common.js";
import { validate_employeeId, validate_role, validate_secret } from "./common.js";
/**
 * validate register data function validates the register data
 * if data is valid it goes to the other middleware/handler else sends the error as the response
 * @param {*} request is the request params send by the user
 * @param {*} response is the response with respect to the request sent by the client
 * @param {*} next if the data is valid it goes to the next middleware/handler
 */
function validate_register_data(request,response,next) {

  // get required params
  const { name, employeeId, phonenumber, email, role, secret} = request.body;

  // check param count
  if (Object.keys(request.body).length != 6) {
    //if required number of params are not there it will send 421 status code
    response.status(406).send({ errors: ["All input fields are required"] });
  }

  // list of required parameters for this call, check param names 
  let key_list = [
    "name",
    "employeeId",
    "role",
    "email",
    "phonenumber",
    "secret",
  ];
  const mismatched_params_array = validate_key_lists(request, key_list);
  if (mismatched_params_array.length != 0) {
    response.status(400).send({errors: mismatched_params_array});
  }

  // check each param if its valid
  let errors_array = [...validate_name(name), ...validate_email(email), ...validate_employeeId(employeeId),...validate_phone_number(phonenumber), ...validate_secret(secret),...validate_role(role)]; 
  console.log(errors_array);


  if (errors_array.length != 0) {
    response.status(404).send({errors: errors_array})
  } else {
    next();
  }

}

export { validate_register_data };