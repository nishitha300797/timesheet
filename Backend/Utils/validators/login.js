import { validate_employeeId ,validate_key_lists,validate_password, validate_role} from "./common.js";

/**
 * validate login data function validates the login details 
 * @param {*} request is the params sent by the user
 * @param {*} response is the response with respect to the user input 
 * @param {*} next if the data is valid it goes to the other middleware/handler
 */
function validate_login_data  (request,response,next){
    const {employeeId,password,role} = request.body

    if (Object.keys(request.body).length != 3) {
        //if required number of params are not there it will send 421 status code
        response.status(406).send({ errors: ["Parameters count is not matches with database fields"] });
      }
      let key_list = [
        "employeeId",
        "password",
        "role"
      ];

      const mismatched_params_array = validate_key_lists(request, key_list);
      if (mismatched_params_array.length != 0) {
        response.status(400).send({errors: mismatched_params_array});
      }
      
      let errors_array = [...validate_employeeId(employeeId), ...validate_password(password),...validate_role(role)]

      if (errors_array.length != 0) {
        response.status(404).send({errors: errors_array})
      } else {
        console.log("validation of createemployee passed!");
        next();
      }
}


export {validate_login_data}