// importing mangoose from mangoose
import mongoose from "mongoose";
// Employee schema

/**
 * creating the employee schema for registering
 * the data and post the details of the employee
 */
const Employee_schema = new mongoose.Schema({
  name: { type: String, required: true },
  employeeId: { type: Number, required: true, unique: true },
  phonenumber: { type: Number, required: true, unique: true },
  role: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  secret: { type: String, required: true },
});

/**
 * creating time sheet schema to post time sheet
 *
 */
const TimeSheetSchema = new mongoose.Schema({
  employeeId: { type: Number, required: true },
  Hours: { type: Number, required: true },
  Description: { type: String, required: true },
  Date: { type: Date, required: true , unique:true},
});

//exporting the schema's
export { Employee_schema, TimeSheetSchema };
