import jwt from "jsonwebtoken";

/**
 * this function is for the authorization of the user
 * it validates the user and gives authorization
 * @param {*} request  is
 * @param {*} response is the response of the function,if the user has a correct jwt token
 * it validates the user
 * @param {*} next
 */

const authenticateToken = (request, response, next) => {
  let jwtToken;
  const authHeader = request.headers["authorization"];
  if (authHeader !== undefined) {
    jwtToken = authHeader.split(" ")[1];
  }
  if (jwtToken === undefined) {
    response.status(401);
    response.send("Invalid Jwt Token");
  } else {
    jwt.verify(jwtToken, "MY_SECRET_TOKEN", async (error, payload) => {
      if (error) {
        response.status(401);
        response.send("Invalid Jwt Token");
      } else {
        request.employeeId = payload.employeeId;
        request.role = payload.role;
        next();
      }
    });
  }
};

/**
 * This checks whether the current role is in ['hr', 'admin]
 */
const checkRoleHR = (request, response, next) => {
  const request_role = request.role;
  const rolesArray = ["hr", "admin"];

  if (!request_role) {
    response.status(401).send("No role recieved!");
  } else {
    if (!rolesArray.includes(request_role)) {
      response.status(401).send("You dont have rights to access this API path");
    } else {
      next();
    }
  }
};

/**
 * This checks whether the current role is in ['admin]
 */
const checkRoleAdmin = (request, response, next) => {
  const request_role = request.role;
  const rolesArray = ["admin"];

  if (!request_role) {
    response.status(401).send("No role recieved!");
  } else {
    if (!rolesArray.includes(request_role)) {
      response.status(401).send("You dont have rights to access this API path");
    } else {
      next();
    }
  }
};

export { authenticateToken, checkRoleAdmin, checkRoleHR };
