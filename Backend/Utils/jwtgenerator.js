// importing jwt from the jsonwebtoken for creating the jwttoken
import jwt from "jsonwebtoken";

/**
 * it generates the jwt_token based on the payload
 * @param employeeId is the employeeId that are passed in login path request body
 * @param role  is the role of the employee
 * @returns it returns a jwt_token for every function call
 */
function jwtgenerator(employeeId, role) {
  //initializing the payload for a set of fields that you want to include in the token being generated
  var payload = {
    employeeId,
    role,
  };
  //initializing jwtToken variable for creating jwt Token
  const jwtToken = jwt.sign(payload, "MY_SECRET_TOKEN");
  return jwtToken;
}
// exporting the jwt_token_generator function
export { jwtgenerator };
