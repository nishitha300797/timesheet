// importing express from the express module
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

// importing post employee details from path handlers ,this function 
// post the employee details into the database
import { createemployee_handler ,login_user_handler ,post_timeSheet_handler } from "./path_handlers/post_api.js";

// these are get api functions importing from path handlers 
import { get_employee_details ,get_time_sheet_handler_by_Id ,get_employee_details_by_id} from "./path_handlers/get_apis.js";
import { validate_register_data } from "./Utils/validators/register.js";
import { validate_login_data } from "./Utils/validators/login.js";

// this validates the data and generates the jwt token
import { authenticateToken, checkRoleAdmin, checkRoleHR } from './Utils/authenticateToken.js'
import { forgot_user_handler}  from "./path_handlers/put_apis.js";

//Creating an instance of express
const app = express();

//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({ origin: "*" }));

/****   API CALLS   ****/

app.post("/login", validate_login_data, login_user_handler);
app.put("/update", forgot_user_handler)

app.post("/addemployee", validate_register_data, createemployee_handler);
app.get("/employeedetails", authenticateToken, checkRoleHR, get_employee_details);
app.get("/employeedetails/:id", authenticateToken, get_employee_details_by_id)

app.post("/timesheet", authenticateToken, post_timeSheet_handler)
app.get("/timesheet/:id", get_time_sheet_handler_by_Id)


/**------------------**/







// this server is listening the port localhost 8000
//connecting the database
mongoose.connect(process.env.MONGOURL)
  .then(() => {
    console.log("Database connected!");
    app.listen(process.env.BACKENDPORT, () => {
      console.log(`server is running at port  ${process.env.BACKENDPORT}!`);
    })
  })
  .catch(() => {
    console.log("Unable to connect to database");
  })
