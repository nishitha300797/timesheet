import { Link, useHistory } from "react-router-dom";
import { useState, useEffect } from "react";
import "./index.css";
import { useSelector, useDispatch } from "react-redux";
import { setLoginData, getProfileData } from "../../redux/actions/employeeActions";

function Login() {
  const dispatch = useDispatch();
  const dataObj = useSelector((state) => state.employeeLogin.loginData);
  const databj = useSelector((state) => state.userProfile.profileData);


  const history = useHistory();
  const [users, setUsers] = useState([]);
  // const [role, setRole] = useState("")
  const [employeeId, setEmployeeId] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  // const [showSubmitError, setShowSubmitError] = useState(false);
  console.log();
  // console.log(users.errorMsg);

  const onSubmitSuccess = () => {
    history.push("/employeehome");
  };

  function onChangeEmployeeId(event) {
    setEmployeeId(event.target.value);
  }

  function onChangePassword(event) {
    setPassword(event.target.value);
  }

  const onSubmitFailure = (data) => {
    setUsers({ showSubmitError: true, errorMsg: data.status_message });
  };

  const submitForm = async (event) => {
    event.preventDefault();
    setUsers({
      employeeId: employeeId,
      password: password,
      role: role,
    });
    const userDetails = { employeeId, password, role };

    if (employeeId === "" || password === "") {
      alert("Please enter the valid Credentials");
    } else {
      const url = "http://localhost:8000/login";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      const response = await fetch(url, options);
      const data = await response.json();

      dispatch(setLoginData(data));

      const empId = data.employeeId;
      const token_jwt = data.jwt_token;

      const profile_url = `http://localhost:8000/employeedetails/${empId}`;
      const option = {
        headers: { Authorization: `Bearer ${token_jwt}` },
        method: "GET",
      };
      const response_data = await fetch(profile_url, option);
      const user_data = await response_data.json();
      dispatch(getProfileData(user_data))


      if (data.status_code !== 200) {
        onSubmitFailure(data);
      } else {
        onSubmitSuccess();
      }
    }
  };

  useEffect(() => {
    submitForm();
  }, []);

  // console.log("Data object: ", dataObj);
  // console.log("Data: ", databj);

  return (
    <>
      <div class="vid-container">
        <video
          id="Video1"
          class="bgvid back"
          autoplay="false"
          muted="muted"
          preload="auto"
          loop
        >
          <source
            src="http://shortcodelic1.manuelmasiacsasi.netdna-cdn.com/themes/geode/wp-content/uploads/2014/04/milky-way-river-1280hd.mp4.mp4"
            type="video/mp4"
          />
        </video>
        <div class="inner-container">
          <video
            id="Video2"
            class="bgvid inner"
            autoplay="false"
            muted="muted"
            preload="auto"
            loop
          >
            <source
              src="http://shortcodelic1.manuelmasiacsasi.netdna-cdn.com/themes/geode/wp-content/uploads/2014/04/milky-way-river-1280hd.mp4.mp4"
              type="video/mp4"
            />
          </video>
          <div class="box">
            <form>
              <h1 className="heading">Login</h1>

              {/* <input type="text" value={role} onChange={event => setRole(event.target.value)} placeholder="Role"/> */}

              <input
                className="input"
                type="text"
                value={role}
                onChange={(event) => setRole(event.target.value)}
                placeholder="Role"
              />
              <input
                className="input"
                type="text"
                value={employeeId}
                onChange={onChangeEmployeeId}
                placeholder="EmployeeId"
              />

              <input
                className="input"
                type="text"
                value={password}
                onChange={onChangePassword}
                placeholder="Password"
              />
              <button onClick={submitForm} type="button">
                Login
              </button>
              <Link to="/forgot">
                <a href="#" className="para1">
                  Forgot Password
                </a>
              </Link>

              <div class="link-password">
                <a href="#">{users.errorMsg}</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
