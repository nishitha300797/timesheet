import { Component } from "react";
import { Link, Redirect, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import "./index.css";
import Profile from "../Profile";
import { useSelector } from "react-redux";
import AddEmployee from "../AddEmployee/index";



function EmployeeHome() {

  const databj = useSelector((state) => state.userProfile.profileData);
 console.log(databj[0].email)

  
  const history = useHistory();
  const loginInfo = useSelector((state) => state.employeeLogin.loginData);
  //console.log("Kfddsjf: ", loginInfo.role)
  const onClickLogout = () => {
    console.log("logout");
    Cookies.remove("jwt_token");

    history.push("/");
    <Redirect to="/dashboard" />
    localStorage.removeItem("persist:Storage");
  };
  
const renderingPage = (login,oooo) => {
  if (login.role === "employee") {
    return(
      <>
        <>
      <div className="header-container">
        <div className="test">
          <input type="checkbox" className="menu-toggle" id="menu-toggle" />
          <div className="mobile-bar">
            <label htmlFor="menu-toggle" className="menu-icon">
              <span />
            </label>
          </div>
          <div className="header">
            <nav>
              <ul>
                <li>
                  <a href="#">{oooo[0].name}</a>
                  
                </li>

                <li>
                  <a onClick={onClickLogout}>Logout</a>
                </li>
              </ul>
            </nav>
          </div>
          <a className="navbar-brand" style={{ padding: 0 }}>
            <img src="https://www.innominds.com/hubfs/logo%20-03.png" />
          </a>
        </div>
      </div>
      <div className="sidebar-container">
        <div className="sidebar">
          <div className="active sidebar-header">
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              height={24}
              width={24}
              xmlns="http://www.w3.org/2000/svg"
            >
              <path fill="none" d="M0 0h24v24H0V0z" />
              <path d="M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H6V6h12v12z" />
            </svg>
            <span>Company</span>
          </div>
          <div className="below">
            <div>
              <div className="sidebar-body">
                <Link to="/profile">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-glyphs/60/000000/test-account.png"
                      className="icon-img"
                    />
                    <span>Profile</span>
                  </div>
                </Link>

                <Link to="/timesheet">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-filled/60/000000/property-time.png"
                      className="icon-img"
                    />
                    <span>Timesheet</span>
                  </div>
                </Link>
                <a href="#">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-glyphs/60/000000/leave.png"
                      className="icon-img"
                    />
                    <span>Requests</span>
                  </div>
                </a>
                <a href="#">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-filled/60/000000/purchase-order.png"
                      className="icon-img"
                    />
                    <span>Payslips</span>
                  </div>
                </a>
                <a href="#">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-glyphs/60/000000/training.png"
                      className="icon-img"
                    />
                    <span>Training Sessions</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
       
        <div className="just">
          <div>
            <h3>Software Products and Digital Engineering</h3>
            Modernize and automate systems and processes, build and launch new
            products faster and optimize development costs; achieve scale and
            agility-leverage our low-code platforms, cloud engineering, and
            microservices architecture
          </div>
          <div>
            <h3>Client success</h3>
            Global construction tools company increases tools usage by 60% with
            tracking software solution from Innominds Innominds builds a
            tracking software that improves tool utilization, and creates a
            building maintenance management solution that results in higher
            customer satisfaction.
          </div>
          <div>
            <h3>
              Accelerate your go-to-market strategy and step up your innovation
              pace
            </h3>
            Leverage iSymphonyTM - our low-code platform and accelerators with
            an intuitive drag-and-drop UI-for rapid application development with
            high-quality and compliable code
          </div>
        
        </div>



        )
      </div>
    </>
      </>
    )
  }
  else if (login.role === "admin"){
    return(
      <>
        <>
      <div className="header-container">
        <div className="test">
          <input type="checkbox" className="menu-toggle" id="menu-toggle" />
          <div className="mobile-bar">
            <label htmlFor="menu-toggle" className="menu-icon">
              <span />
            </label>
          </div>
          <div className="header">
            <nav>
              <ul>
                <li>
                  <a href="#">{oooo[0].name}</a>
                  
                </li>

                <li>
                  <a onClick={onClickLogout}>Logout</a>
                </li>
              </ul>
            </nav>
          </div>
          <a className="navbar-brand" style={{ padding: 0 }}>
            <img src="https://www.innominds.com/hubfs/logo%20-03.png" />
          </a>
        </div>
      </div>
      <div className="sidebar-container">
        <div className="sidebar">
          <div className="active sidebar-header">
            <svg
              stroke="currentColor"
              fill="currentColor"
              strokeWidth={0}
              viewBox="0 0 24 24"
              height={24}
              width={24}
              xmlns="http://www.w3.org/2000/svg"
            >
              <path fill="none" d="M0 0h24v24H0V0z" />
              <path d="M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H6V6h12v12z" />
            </svg>
            <span>Company</span>
          </div>
          <div className="below">
            <div>
              <div className="sidebar-body">
                <Link to="/profile">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-glyphs/60/000000/test-account.png"
                      className="icon-img"
                    />
                    <span>Profile</span>
                  </div>
                </Link>

                <Link to="/addemployee">
                  <div>
                    <img
                      src="https://img.icons8.com/ios-filled/60/000000/property-time.png"
                      className="icon-img"
                    />
                    <span>Add Employee</span>
                  </div>
                </Link>
               
              </div>
            </div>
          </div>
        </div>
        <div className="just">
          <div>
            <h3>Software Products and Digital Engineering</h3>
            Modernize and automate systems and processes, build and launch new
            products faster and optimize development costs; achieve scale and
            agility-leverage our low-code platforms, cloud engineering, and
            microservices architecture
          </div>
          <div>
            <h3>Client success</h3>
            Global construction tools company increases tools usage by 60% with
            tracking software solution from Innominds Innominds builds a
            tracking software that improves tool utilization, and creates a
            building maintenance management solution that results in higher
            customer satisfaction.
          </div>
          <div>
            <h3>
              Accelerate your go-to-market strategy and step up your innovation
              pace
            </h3>
            Leverage iSymphonyTM - our low-code platform and accelerators with
            an intuitive drag-and-drop UI-for rapid application development with
            high-quality and compliable code
          </div>
        
        </div>
       



        )
      </div>
    </>
      </>
    )
  } 
}

 const funct = renderingPage(loginInfo,databj)
  return (
   <>{funct}</>
  );
}

export default EmployeeHome;
