import "./index.css";
import { Link, useHistory } from "react-router-dom";
import { useState, useContext } from "react";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom";

function Forgot() {
  const history = useHistory();
  const [users, setUsers] = useState([]);
  const [employeeId, setEmployeeId] = useState("");
  const [secret, setSecret] = useState("");
  const [newpassword, setNewpassword] = useState("");
  const [showSubmitError, setShowSubmitError] = useState(false);
  // console.log(password)

  const onSubmitSuccess = () => {
    history.push("/");
  };

  const onSubmitFailure = (data) => {
    setUsers({ showSubmitError: true, errorMsg: data.status_message });
  };

  const submitForm = async (event) => {
    event.preventDefault();
    // console.log("working")
    setUsers({
      employeeId: employeeId,
      secret: secret,
      newpassword: newpassword,
    });
    const userDetails = { employeeId, secret, newpassword };
    if (employeeId === "" || newpassword === "" || secret === "") {
      alert("Please enter the valid Credentials");
    } else {
      const url = "http://localhost:8000/update";
      const options = {
        method: "PUT",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      const response = await fetch(url, options);
      const data = await response.json();
      console.log(data);
      if (data == "password updated successfully") {
        onSubmitSuccess();
      }
    }
  };

  return (
    <div className="login-page">
      <div className="form">
        <h3>Reset your password</h3>
        <form className="login-form">
          <input
            type="text"
            placeholder="Employee id"
            value={employeeId}
            onChange={(event) => setEmployeeId(event.target.value)}
          />
          <input
            type="text"
            placeholder="secret"
            value={secret}
            onChange={(event) => setSecret(event.target.value)}
          />
          <input
            type="password"
            placeholder="New Password"
            value={newpassword}
            onChange={(event) => setNewpassword(event.target.value)}
          />
          <button type="submit" onClick={submitForm}>
            Reset
          </button>
        </form>
      </div>
      <div>
        <img
          className="image"
          src="https://i.ytimg.com/vi/QN58yl2ZZgA/maxresdefault.jpg"
        />
      </div>
    </div>
  );
}

export default Forgot;
