import React from "react";
import { useState } from "react";
import "./index.css";
// import { useHistory } from "react-router-dom"

function AddEmployee() {
  const [object, setObject] = useState([]);
  // const history = useHistory();
 

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const [role, setRole] = useState("");
  const [secret, setSecret] = useState("");
  const [employeeId, setEmployeeId] = useState("");

  //  const redirectToSignin = ()=>{

  //   history.replace("/")
  // }

  const Submitfrom = async (event) => {
    event.preventDefault();
    setObject({
      name: name,
      email: email,
      phonenumber: phonenumber,
      role: role,
      secret: secret,
      employeeId: employeeId,
    });
    const userDetails = {
      name,
      email,
      phonenumber,
      role,
      secret,
      employeeId,
    };

    const url = "http://localhost:8000/addemployee";
    const options = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options);
    const data = await response.json();
    console.log(data);

    if (data.status_code !== 200) {
      setObject({ error_msg: data.status_message, is_signup: true });
    } else {
      setName("");
      setEmail("");
      setEmployeeId("");
      setPhonenumber("");
      setRole("");
      setSecret("");
    }
  };

  return (
    <>
      <div className="col-lg-12">
        <div className="jumbotron">
          <div className="">
            <h1> Add Employee </h1>
          </div>
        </div>
        <div className="">
          <div className="row">
            <div className="col-lg-12">
              <div className="panel panel-primary">
                <div className="panel-heading">
                  <h3 className="panel-title">
                    <strong>Add Employee</strong>
                  </h3>
                </div>
                <div className="panel-body">
                  <form onSubmit={Submitfrom} id="form">
                    <div className="form-group">
                      <label htmlFor="name">Employee Name: </label>
                      <input
                        type="text"
                        className="form-control"
                        id="employeeNameInput"
                        value={name}
                        onChange={(event) => setName(event.target.value)}
                        placeholder="name"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name">Employee Id: </label>
                      <input
                        type="text"
                        className="form-control"
                        id="employeeIdInput"
                        value={employeeId}
                        onChange={(event) => setEmployeeId(event.target.value)}
                        placeholder="employee id"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="empRole">Email:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="roleInput"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                        placeholder="Email"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="empRole">Phone Number:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="roleInput"
                        maxLength={10}
                        minLength={10}
                        value={phonenumber}
                        onChange={(event) => setPhonenumber(event.target.value)}
                        placeholder="Phone Number"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="empRole">Role:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="roleInput"
                        value={role}
                        onChange={(event) => setRole(event.target.value)}
                        placeholder="role"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="empRole">secret:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="roleInput"
                        value={secret}
                        onChange={(event) => setSecret(event.target.value)}
                        placeholder="Your secret"
                      />
                    </div>
                    <button
                      type="submit"
                      className="btn btn-primary"
                      id="addEmployeeBtn"
                    >
                      Submit
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AddEmployee;
