import './index.css'
const NotFound = () => (
    <div className="not-found-container">
      <img
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqI3lHFjBbLelg5rGnkZVukHUI2cd9cnEGOQ&usqp=CAU"
        alt="not found"
        className="not-found-img"
      />
    </div>
  )
  
  export default NotFound
  