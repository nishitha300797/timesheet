import "./index.css"
import {useState} from "react"

function Timesheet() {

const [Timesheet, setTimesheet] = useState([])  
const [date, setDate] = useState("")
const [hours,  setHours] = useState("")
const [description, setDescription] = useState("")


    return (
<>
<table>
  <thead>
    <tr>
      <th>Date</th>
      <th>Working Hours</th>
      <th>Description</th>
      <th>Save</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <strong>
          <input type="date" 
          value={date}
          onChange = {(event) => setDate(event.target.value)}/>
        </strong>
      </td>
      <td>
        <input type="number"
         value={hours}
         onChange = {(event) => setHours(event.target.value)} />
      </td>
      <td>
        <input type="text"
         value={description}
         onChange = {(event) => setDescription(event.target.value)} />
      </td>
      <td>
        <button class="btn">Save</button>
      </td>
    </tr>
  </tbody>
</table>

</>
    );
  }
  
  export default Timesheet;