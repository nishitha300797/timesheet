import { useState,useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./index.css";

function Profile() {

  const databj = useSelector((state) => state.userProfile.profileData);
 
  return (
    <>
      <div class="profile-container">
        <div class="profile-card">
          <h3 class="card-heading">Profile Picture</h3>
          <div>
            <img
              class="img-account-profile"
              src="https://img.freepik.com/free-vector/smiling-girl-avatar_102172-32.jpg"
              alt=""
            />
          </div>
          <div>
            <button class="btn" type="button">
              Upload new image
            </button>
          </div>
        </div>
        <div class="profile-details">
          <h3 class="card-heading">Details</h3>
          <div>
            <form>
              <div class="input-container">
                <label for="inputUsername">Employee Id: </label>
                <p class="form-input">{databj[0].employeeId}</p>
              </div>

              <div class="row">
                <div class="input-container">
                  <label for="inputFirstName">Employee Name: </label>
                  <p class="form-input">{databj[0].name}</p>
                </div>               
              </div>
                <div class="input-container">
                  <label for="inputFirstName">Email:</label>
                  <p class="form-input">{databj[0].email}</p>
                </div>
                <div class="input-container">
                  <label for="inputFirstName">Mobile Number:</label>
                  <p class="form-input">{databj[0].phonenumber}</p>
                </div>


              <button class="btn" type="button">
                Save changes
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;
