import {Redirect, Route} from 'react-router-dom'
// import Login from '../Login'
import { useSelector } from 'react-redux'


const ProtectedRoute = props => {
  const loginData = useSelector((state) => state.employeeLogin.loginData)
  
  const token = loginData.jwt_token
  if (token === undefined) {
    return <Redirect to="/" />
  }
  return <Route {...props} />
}

export default ProtectedRoute
