import { ActionTypes } from "../contants/action-types";

export const setLoginData = (loginData) => {
  return {
    type: ActionTypes.SET_LOGIN_DATA,
    payload: loginData,
  };
};


export const getProfileData = (profileData) => {
  return {
    type: ActionTypes.GET_PROFILE_DATA,
    payload: profileData,
  };
};



