export const ActionTypes = {
    SET_LOGIN_DATA: "SET_LOGIN_DATA",
    GET_PROFILE_DATA: "GET_PROFILE_DATA"
}