import { ActionTypes } from "../contants/action-types";

const initialState = {
  loginData: [],
 
  
};

const userState = {
  profileData : [],
}



export const employeeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_LOGIN_DATA:
      return { ...state, loginData: payload };

    default:
      return state;
  }
};


export const userProfileReducer = (state = userState, { type, payload }) => {
  switch (type) {
    case ActionTypes.GET_PROFILE_DATA:
      return { profileData: payload };

    default:
      return state;
  }
};


