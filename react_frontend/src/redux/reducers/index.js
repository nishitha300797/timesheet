import { combineReducers } from "redux";

import { employeeReducer , userProfileReducer} from "./employeeReducer";

const reducers = combineReducers({
  employeeLogin: employeeReducer,
  userProfile: userProfileReducer
});

export default reducers;
