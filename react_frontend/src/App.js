import "./App.css";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

import EmployeeHome from "./components/EmployeeHome";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Timesheet from "./components/Timesheet";
import AddEmployee from "./components/AddEmployee";
import ProtectedRoute from "./components/ProtectedRoute";
import Forgot from "./components/Forgot/index";
import NotFound from "./components/Notfound";

function App() {
  const loginInfo = useSelector((state) => state.employeeLogin.loginData);
  const renderingPage = (login) => {
    if (login.role === "employee") {
      return (
        <>
          <ProtectedRoute exact path="/timesheet" component={Timesheet} />
          <ProtectedRoute exact path="/employeehome" component={EmployeeHome} />
          <ProtectedRoute exact path="/profile" component={Profile} />
        </>
      );
    } else if (login.role === "admin") {
      return (
        <>
          <ProtectedRoute exact path="/addemployee" component={AddEmployee} />
          <ProtectedRoute exact path="/employeehome" component={EmployeeHome} />
          <ProtectedRoute exact path="/profile" component={Profile} />
        </>
      );
    }
  };
  const pageData = renderingPage(loginInfo);
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/forgot" component={Forgot} />
          <ProtectedRoute exact path="/timesheet" component={Timesheet} />
          <ProtectedRoute exact path="/addemployee" component={AddEmployee} />
          <ProtectedRoute exact path="/employeehome" component={EmployeeHome} />
          <ProtectedRoute exact path="/profile" component={Profile} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
