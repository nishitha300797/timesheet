/**
 * To validate the employee id given by the user for registering
 * @param {} employeeId to be validated
 * @returns an array of strings of error messages
 */
 function validate_employeeId(employeeId) {
  let returnArray = [];

  if (!employeeId) {                       // check if parameter is sent
    returnArray.push("Employee Id has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof(employeeId) != 'string') {    // check if param is string
    returnArray.push("Employee Id parameter sent is not a string " + employeeId);
    return returnArray; 
  }
  if (!employeeId.match(/^[0-9]{5}$/)) {
                                          // check if param is valid format
    returnArray.push("Employee Id is not in the right format it includes" + employeeId);
    return returnArray;
  }

  return returnArray;
}


function validate_password(password) {
  let returnArray = [];

  if (password === undefined) {                        // check if parameter is sent
    returnArray.push("pasword has not been sent. This is mandatory.");
    return returnArray;
  }
  if (typeof(password) != 'string') {                  // check if param is string
    returnArray.push("password parameter sent is not a string " + password);
    return returnArray; 
  }
  if (!password.match(/^[a-zA-Z0-9!@#$%^&*()_+]{6,16}$/)) {
                                                    // check if param is valid format
    returnArray.push("password is not in the right format " + password);
    return returnArray;
    }
    return returnArray;
}


export {validate_employeeId,validate_password}